\label{index_documentation}%
\hypertarget{index_documentation}{}%
 ~\newline
 {\bfseries  Potential-\/density pair calculator for 3\+D, initially isothermal, self-\/gravitating gas discs with or without an external potential }

~\newline
 {\bfseries  P\+U\+R\+P\+O\+S\+E }

This program calculates a self-\/consistent potential-\/density pair for a 3\+D, self-\/gravitating isothermal, axisymmetric gas disc with a prescribed {\itshape surface} density profile in the radial direction ( $ R $), in hydrostatic equilibrium in the vertical direction ( $ z $) and centrifugal support, with or without a static external potential, following the ideas outlined in \cite{wan10a}. For alternative approaches see e.\+g. \cite{spr05c}; \cite{sut07a}; \cite{par13a}.

~\newline
 {\bfseries  U\+S\+E }

The output of this program is intended to be used as initial conditions for numerical simulations of (galactic) gas discs which require an initial state in perfect equilibrium.

~\newline
 {\bfseries  T\+H\+E\+O\+R\+E\+T\+I\+C\+A\+L B\+A\+C\+K\+G\+R\+O\+U\+N\+D }

Briefly, if $ R = \sqrt{x^2 + y^2} $ and $ z $ are the canonical cylindrical coordinates, the gas {\itshape volume} density and its corresponding (total) potential pair at equilibrium are related by

\[ \rho_g(R,z) = \rho_R(R) \exp{ \left[ - \frac{ \psi_z }{ c_s^2 } \right] } \, . \]

Here,

\[ \psi_z(R,z) = \psi(R,z) - \psi(R,0) \]

is the potential {\itshape difference} in the vertical direction, and the total potential $ \psi(R,z) $

\[ \psi = \psi_g + \psi_{ext} \]

is the sum of the gas disc potential and the external potential (which may be zero everywhere, or correspond to various components\+: D\+M halo, stellar disc, bulge).

The gas {\itshape volume} density in the equatorial plane ( $ z = 0 $) is given by

\[ \rho_R(R) \equiv \rho_g(R,z=0) = \frac{ 1 }{ S } ~\Sigma_g(R) \, . \]

where $ \Sigma_g(R) $ is the {\bfseries target} {\itshape surface} density of the gas disc. In principle, any well behaved function of $ R $ may be specified, although here it is assumed that the disc\textquotesingle{}s surface density follows an exponential law\+:

\[ \Sigma_g(R) = \Sigma_0 \exp{ \left[ - \frac{ R }{ R_d } \right] } \, . \]

The quantity $ S $ given above is the normalisation of the surface density and is given by (the second step is assumed to be correct!)

\begin{eqnarray*} S & = & \int_{-\infty}^{\infty} \exp[ -\psi_z / c_s^2 ] ~dz \\ & = & 2 \int_{0}^{\infty} \exp[ -\psi_z / c_s^2 ] ~dz \, , \end{eqnarray*}

and

\[ c_s^2 = (\gamma - 1) ~e(T) = \frac{ k_B T }{ \mu m_p } \]

is the isothermal sound speed in the gas; $ \mu $ is the mean molecular weight; $ R_d $ is the radial scale length of the disc and $ \Sigma_0 $ is the surface density in the equatorial plane and $ R = 0 $, i.\+e. the disc\textquotesingle{}s centre. The gas pressure and the gas density are related by an ideal equation of state

\[ p_g = c_s^2 ~\rho_g \, . \]

Once $ \psi_z(R,z) $ is known, the total gas potential may be approximated by (see \cite{bin08a}; their equation 2.\+75a )

\[ \psi(R,z) = \psi_z(R,z) + \psi(R,0) \, .\]

Note that in the absence of an external potential, the solution will depend on $ R_d $, $ c_s $, and $ \Sigma_0 $ in a trivial way; in this case, the latter quantities become mere scaling factors, which may be factored out to obtain a general (i.\+e. {\itshape scale-\/free}) solution (for a chosen surface density profile and radial scale length; see below). This is not longer true if an external potential is included, and the solution will be specific to the chosen parameter values.

~\newline
 {\bfseries  M\+E\+T\+H\+O\+D }

Given a prescribed {\itshape surface} gas density and a total external potential, the code {\ttfamily disc\+\_\+poisson\+\_\+solver} integrates the reduced Poisson equation

\[ \frac { d^2 }{ dz^2 } \psi_{g,z} = 4 \pi G ~\rho_R(R) ~\exp{ \left[ - \frac{ \psi_z }{ c_s^2 } \right] } \]

for the {\itshape gas} potential difference

\[ \psi_{g,z}(R,z) = \psi_g(R,z) - \psi_g(R,0) \, , \]

using the adaptive step-\/size 5th order R\+K method, using appropriate boundary conditions (see below). Note that the above approximation -- i.\+e. ignoring the radial derivatives in Poisson\textquotesingle{}s equation -- is only valid for a thin disc (see \cite{wan10a}; their Appendix E).

Let \begin{eqnarray*} z_0 = \frac{ c_s^2 }{ 2 \pi G \Sigma_0 } & \qquad & \textnormal{ : vertical scale } \\ \tilde{R} = \frac{ R }{ R_d } & \qquad & \textnormal{ : scaled radial coordinate } \\ x = \frac{ z }{ z_0 } & \qquad & \textnormal{ : scaled vertical coordinate } \\ W = -\frac{ \psi_{g,z} }{ c_s^2 } & \qquad & \textnormal{ : scaled gas potential difference } \\ f_{\rho} = \frac{ 1 }{ \tilde{S} } \exp[ -\tilde{R} ] & \qquad & \textnormal{ : scaled gas density at z=0 } \\ \tilde{S} = \frac{ S }{ 2 z_0 } & \qquad & \textnormal{ : scaled surface-density normalisation } \end{eqnarray*}

Note that all the scaled quantities are dimensionless, while $ z_0 $ has dimensions of length (cf. \cite{spi42a}; their equation 40)

If the disc is fully {\itshape self-\/gravitating}, i.\+e. if there is no external potenial, then $ \psi_z = \psi_{g,z} $, and with these definitions, then the reduced Poisson equation can be recast into the following form\+:

\[ \frac{ d^2 }{ dx^2 } W = -f_{\rho}(\tilde{R}) ~\exp[ W ] \, . \]

In the presence of a (composite) external potential $ \psi_{ext} $ -- due to, e.\+g. a D\+M halo or a stellar disc --, the reduced Poisson equation reads instead

\[ \frac{ d^2 }{ dx^2 } W = -f_{\rho}(\tilde{R}) ~\exp[ W ] ~\exp{ \left[ - \frac{ \psi_{ext,z} }{ c_s^2 } \right] } \, , \]

where the external potential difference

\[ \psi_{ext,z}(R,z) = \psi_{ext}(R,z) - \psi_{ext}(R,0) \, . \]

To solve the first equation numerically, it may be transformed into a system of three first-\/order differential equations with three unknowns,

\begin{eqnarray*} W_1 & = & W \\ W_2 & = & \frac{ dW_1 }{ dx } \\ W_3 & = & \exp{ [ W_1 ] } \end{eqnarray*}

which imply

\begin{eqnarray*} \frac{ d }{ dx } W_1 & = & W_2 \phantom{-f_{\rho}(\tilde{R})~W_3} \qquad \textnormal{ (by definition) } \\ \frac{ d }{ dx } W_2 & = & -f_{\rho}(\tilde{R}) ~W_3 \phantom{W_2} \qquad \textnormal{ (this is the reduced Poisson equation) } \\ \frac{ d }{ dx } W_3 & = & W_3 ~W_2 \phantom{~-f_{\rho}(\tilde{R}} \qquad \textnormal{ (by definition of W2 and W3) } \end{eqnarray*}

subject to the boundary conditions (initial values)\+:

\begin{eqnarray*} W_1(x=0) & = & 0 \qquad \textnormal{ : potential vanishes in the equatorial plane } \\ \frac{ d }{ dx } W_1(x=0) & = & 0 \qquad \textnormal{ : force vanishes in the equatorial plane } \\ W_3(x=0) & = & 1 \qquad \textnormal{ : density in the equatorial plane (assuming the scaled S = 1) } \end{eqnarray*}

In reality, all $ W $\textquotesingle{}s are functions of $ \tilde{R} $, $ x $, and the numerical integration over $ x $ is best achieved by fixing $ \tilde{R} $, for a range of $ \tilde{R} $\textquotesingle{}s. Thus, the result is a series of values for $ W_1 $, $ \exp{ [ W_1 ] } $, and $ f_{\rho} $ on a grid $ (\tilde{R},x) $.

To solve the second equation numerically, it is transformed into a system of three first-\/order differential equations with three unknowns,

\begin{eqnarray*} W_1 & = & W \\ W_2 & = & \frac{ W_1 }{ dx } \\ W_3 & = & \exp{ [ W_1 ] } \end{eqnarray*} and \begin{eqnarray*} \frac{ d }{ dx } W_1 & = & W_2 \\ \frac{ d }{ dx } W_2 & = & -f_{\rho}(\tilde{R}) ~W_3 ~\exp[ -\psi_{ext,z} / c_s^2 ] \\ \frac{ d }{ dx } W_3 & = & W_3 ~W_2 \end{eqnarray*}

subject to the boundary conditions (initial values)\+:

\begin{eqnarray*} W_1(x=0) & = & 0 \qquad \textnormal{ potential vanishes in the equatorial plane } \\ \frac{ d }{ dx } W_1(x=0) & = & 0 \qquad \textnormal{ force vanishes in the equatorial plane } \\ W_3(x=0) & = & 1 \qquad \textnormal{ density in the equatorial plane (assuming the scaled S = 1) } \end{eqnarray*}

A key point is to bear in mind is that, in either case, neither the gas volume density nor the gas potential are know beforehand. An {\itshape iterative} process is thus necessary which involves solving for one first and then for the other, until a self-\/consistent solution is found which satisfies the constrain that the resulting gas surface density is close enough to the target surface density, starting from a guess for the gas volume density. Following \cite{wan10a}, we start with the assumption that \{S\} = 1, i.\+e. $ f_{\rho} = \exp[ -\tilde{R} ] $ initially.

It is also worth emphasising that, in the case of a self-\/gravitating disc ( $ \psi_{ext} = 0 $), the scaling introduced above on all variables, in particular the radial coordinate R and the vertical distance (integration variable) $ z $, $ z_0 $, is extremely convenient, since it allows to solve for the potential and density only once (for a given surface-\/density function), and scale the result to different scale lengths, central densities, and temperatures as required. The initial conditions of a disc in a simulation can be easily computed by interpolation the potential and density over the properly scaled $ (R,z) $-\/grid. This is no longer possible if an external potential is present, and the initial conditions will be particular of that potential, as well as the gas parameters. Note that the output will be given on the scaled grid. In other words, the output coordinates need to be rescaled by $ R_d $ and $ z_0 $ when generating initial conditions. Also, note that some of the output quantities, notably Toomre\textquotesingle{}s Q parameter and the total (approximated) potential will depend on $ R_d $, $ \Sigma_0 $ and $ c_s^2 $.

~\newline
 {\bfseries  U\+S\+E\+R I\+N\+P\+U\+T P\+A\+R\+A\+M\+E\+T\+E\+R\+S }

Users may specify the desired properties of the system through a list of C pre-\/processor definitions (for examples, see files in directory \hyperlink{dir_f0c4089d8e9cb885ea42a1a3e4fbd4af}{input\+\_\+params}). Currently, only exponential surface density profiles are considered; two different D\+M halo potentials (N\+F\+W; Hernquist) and only one stellar-\/disc potential (Miyamoto-\/\+Nagai) have been implemented. However, other surface density profiles and potentials may be easily included. Note that cgs units are assumed throughout.

User defined parameters can be classified into\+:

{\bfseries  Physical parameters } 
\begin{DoxyItemize}
\item mass unit and length unit  
\item central surface density (or disc mass), temperature, scale length  
\item the external potential\+:  
\begin{DoxyItemize}
\item D\+M halo\+: N\+F\+W / Hernquist, defined by its total mass and scale radius  
\item stellar disc\+: Miyamoto-\/\+Nagai, defined by the total mass of the stellar disc, scale length and scale height)  
\end{DoxyItemize}
\item the extension of the {\itshape scaled} (R,z)-\/grid and resolution (along each dimension)  
\end{DoxyItemize}

{\bfseries  Numerical parameters } 
\begin{DoxyItemize}
\item the extension and resolution of the integration range  
\item the maximum number of iterations and the convergence level for the iterative process  
\end{DoxyItemize}

When setting the range in $ \tilde{R} $ and $ x $, it should be kept in mind that ideally, the corresponding range in $ R $ and $ z $ covers the simulation volume of interest.

\begin{DoxyAttention}{Attention}
Although the numerical parameters can in principle be modified, the should not be modified unless there is a good reason to do so, and the user knows why / how to choose appropriate values.
\end{DoxyAttention}
~\newline
 {\bfseries  A note on the choice of the integration range for $ x $ to solve the reduced Poisson equation }

The vertical scale in physical units (cgs) can be expressed as

\begin{eqnarray*} z_0 & = & \frac{ 1 }{ 2 \pi G } ~c_s^2 ~\frac{ 1 }{ \Sigma_0 } \\ & \sim & 2 \times 10^{20} {\rm ~cm} ~\left( \frac{ c_s }{ 10 {\rm ~kms^{-1}} } \right)^2 ~\left( \frac{ 10^{-2} {\rm ~g}/{\rm ~cm}^2 }{ \Sigma_0 } \right) \, . \end{eqnarray*}

Hence, for a mean molecular weight $ \mu = 1 $, a gas temperature of $ 10^4 $ K -- which result in a sound speed of roughly $ 10 {\rm ~kms^{-1}} $--, and a surface density at the disc\textquotesingle{}s centre of $ 5 \times 10^7 {\rm ~M_{\odot}} / {\rm ~kpc}^2 \sim 10^{-2} {\rm ~g}/{\rm ~cm}^2 $ \cite{deh98b}, the scale height at $ (R,z)=(0,0) $ is $ z_0 \sim 2 \times 10^{20} {\rm ~cm} = 0.06 {\rm ~kpc} $.

Here, we integrate the reduced Poisson equation in the vertical direction out to a {\itshape scaled} vertical distance $ x_{max} = 10^6 $, which using the above physical parameters implies a maximum distanced from the disc plane of $ \sim 60 {\rm ~Mpc} $. Thus, our upper limit of integration can be safely considered to be infinite for all practical purposes, or at least large enough for the numerical solution to be a good approximation to the true solution (within the assumptions that lead to the reduced Poisson equation above).

~\newline
 {\bfseries  O\+U\+T\+P\+U\+T }

The output of the code (directed to {\ttfamily stdout}) consists of the following blocks of values (separated by two blank lines and a line starting with \textquotesingle{}\#\textquotesingle{})\+:
\begin{DoxyEnumerate}
\item The scaled density given by $ \tilde{\rho} = f_{\rho}(\tilde{R}) ~\exp[W_1] $ on a grid $ (\tilde{R}, x) $. The physical gas density at $ (R,z) $ is recovered by scaling the density values on the grid by $ (\Sigma_0 / 2 z_0) $, i.\+e. \[ \rho(R,z) = \frac{ \Sigma_0 }{ 2 z_0 } ~\tilde{\rho} \, , \] and the $ \tilde{R} $ and $ x $ coordinate values by $ R_d $ and $ z_0 $, respectively.
\item A table, as a function of $ \tilde{R} $, of
\begin{DoxyEnumerate}
\item Column 2 / 3\+: The {\itshape scaled} rotation velocity due to the pressure support, \[ V^2_p = \left. \frac{ R }{ \rho_g } \frac{ \partial \rho_g }{ \partial R } \right\vert_{z=0} \] and the {\itshape scaled} rotation velocity (squared) due to the gas potential, \[ V^2_g = \left. R \frac{ \partial\psi_g }{ \partial R } \right\vert_{z=0} \, , \] in the equatorial plane as a function of $ \tilde{R} $. The actual velocities on the $R$-\/grid are simply recovered by scaling its output value by $ c_s^2 $ and $ (4 \pi G \Sigma_0 R_d) = (2 c_s^2 R_d / z_0) $, respectively, and the scaled radial coordinate by $ R_d $. Note that generally $ V^2_p < 0 $.
\end{DoxyEnumerate}
\item In the same block as 2., the output gives\+:
\begin{DoxyEnumerate}
\item Column 4\+: The {\itshape physical} circular velocity due to the {\itshape external} potential (if included; otherwise 0), \[ V^2_{\psi} = \left. R \frac{ \partial \psi_{ext} }{ \partial R } \right\vert_{z=0} \, , \] in the equatorial plane. No scaling is required since the above is given in physical units ( ${\rm cm^2 / s^2} $);
\item Columns 5 / 6\+: The scaled gas density and scaled gas potential in the equatorial plane, $ \tilde{\rho}(R,0) $, $ W_1(R,0) $; their actual physical values are recovered by scaling their output values by $ (\Sigma_0 / 2 z_0) $ and $ c_s^2 $, respectively. Note that $ W_1(0,0) \equiv 0 $ and $ W_1(R,0) < 0 $ for $ R > 0$
\item Column 10\+: The integral of the volume density (which may be compared to the {\itshape scaled} target surface density), $ \Sigma_g / \Sigma_0 $;
\item Column 11\+: Toomre\textquotesingle{}s stability parameter in physical units, \[ Q(R) \equiv \frac{ c_s \, \kappa(R) }{ \pi G \, \Sigma_g(R) } \, , \] where the {\itshape epicyclic frequency} $ \kappa $ is calculated using \[ \kappa^2(R) \equiv \left.\frac{ 3 }{ R } \frac{ \partial \psi }{ \partial R } \right\vert_{z=0} + \left.\frac{ \partial^2 \psi }{ \partial R^2 }\right\vert_{z=0} \, . \] Note that all the above quantities are given as a function of $ \tilde{R} $, which needs to be rescaled by $ R_d $ in order to obtain the corresponding values at $ R $.
\end{DoxyEnumerate}
\item With the same structure as block 1., the total (approximated) gas disc {\itshape physical} potential given by \[ \psi_g(R,z) = \psi_{g,z}(R,z) + \psi_g(R,0) \, .\] Note that $ \psi_g(R,z) < 0 $ for $ R,z > 0 $ with $ \psi_g(0,0) \equiv 0 $.
\end{DoxyEnumerate}

The total rotational (azimuthal) velocity (squared) is given by the sum of all the individual, properly scaled components,

\[ V^2_{tot} = V^2_{g} + V^2_{p} \, \left[ + V^2_{\psi} \right] \, .\]

The last term can obviously be ignored if no external potential is included.

It is interesting to mention that, while $ V^2_g $ can be computed analytically for the case of an isothermal, self-\/gravitating gas disc (see \cite{fre70a}; their eq. 12) -- the approach followed by \cite{wan10a} --, here we compute $ V^2_g $ by numerically calculating the potential generated by the gas density in the equatorial plane using equation 39 of \cite{cud93a}. The result is accurate to 1 percent when compared to the analytic solution for s\+R\+\_\+\+G\+R\+I\+D\+\_\+\+S\+T\+E\+P = 0.\+05, and improves with decreasing s\+R\+\_\+\+G\+R\+I\+D\+\_\+\+S\+T\+E\+P (show plot!).

~\newline
 {\bfseries  C\+O\+D\+E V\+E\+R\+I\+F\+I\+C\+A\+T\+I\+O\+N($\ast$) }

The code can be verified at least in two different ways\+: 1) Check whether an isothermal, self-\/gravitating gas disc (with / without external potential) initialised in this way is stable over secular time scales; in principle, this can be tested for using any suitable hydrodynamics grid-\/ based code; 2) Comparison of the numerical results to their analytic counterpart (whenever existent). In the first case, it is perhaps a good a idea to perform a direct comparison to the results of the original paper \cite{wan10a}, although we also consider a case not handled in there. For a comparison as close as posible, we follow the original paper and use the M\+H\+D A\+M\+R code R\+A\+M\+S\+E\+S \cite{tey02a}. In the second case, we may compare directly to the results by \cite{spi42a} and \cite{fre70a}. The validation tests considered here are then as follows\+:


\begin{DoxyEnumerate}
\item Stability of a self-\/gravitating gas disc with no external potential ({\itshape Spitzer-\/\+Freeman} gas disc)
\begin{DoxyEnumerate}
\item Comparison of the radial density profile expected for an exponential density profile with scale length $ R_d $.
\item Comparison of the vertical density profile obtained by the code to the Spitzer (1942) $ {\rm sech}^2 $ density profile.
\item Comparison of the gas rotation velocity obtained by the code to the Freeman (1970) velocity profile.
\item Check of Toomre\textquotesingle{}s stability criterium\+: $ Q(R) > 1 \, ~\forall R $. ~\newline
\href{../../../../../codes/ramses/trunk/doc/html/mwdisc_spitzer_freeman_3_d_hd.html}{\tt {$\rightarrow$} Go to the test}
\end{DoxyEnumerate}
\item Stability of a self-\/gravitating gas disc embedded in a static, spherical D\+M halo potential (cf. model {\ttfamily Gas0} in \cite{wan10a})
\begin{DoxyEnumerate}
\item Toomre parameter as a function of R
\item Surface density profile initially and at later times
\item Rotation velocity curve initially and at later times ~\newline
\href{../../../../../codes/ramses/trunk/doc/html/mwdisc_gas_dmhalo_3_d_hd.html}{\tt {$\rightarrow$} Go to the test}
\end{DoxyEnumerate}
\item Stability of a self-\/gravitating gas disc embedded in a static, composite potential consisting of a spherical D\+M halo and a stellar disc (model {\ttfamily Gas\+Star}, similar to {\ttfamily Gas\+Star1} in \cite{wan10a})
\begin{DoxyEnumerate}
\item Toomre parameter as a function of $ R $
\item Surface density profile initially and at later times
\item Rotation velocity curve initially and at later times ~\newline
\href{../../../../../codes/ramses/trunk/doc/html/mwdisc_gas_halo_sdisc_3_d_hd.html}{\tt {$\rightarrow$} Go to the test}
\end{DoxyEnumerate}
\end{DoxyEnumerate}

\begin{DoxyNote}{Note}
($\ast$) See Calder et al. (2002) \cite{cal02a} for formal definitions of \char`\"{}verification\char`\"{}, \char`\"{}validation\char`\"{}, etc.
\end{DoxyNote}
\begin{DoxyAuthor}{Author}
Thor Tepper Garcia (\href{mailto:tepper@physics.usyd.edu.au}{\tt tepper@physics.\+usyd.\+edu.\+au})
\end{DoxyAuthor}
\begin{DoxyDate}{Date}
Jul, 2016 
\end{DoxyDate}
