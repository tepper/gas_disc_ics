// provide various definitions (parameters, functions) of cosmological quantities

#include <math.h>
#include "cosmo.h"
#include "const.h"

// function declarations
double radius_vir(double m_vir, double z)
{ // calculates and returns the virial radius (in cm) given a virial mass m_vir (in g) of a halo at redshift z

	double density_vir;

	density_vir = overdensity_vir(z) * RHO_CRIT_0(100. * LITTLE_H * sqrt(E_SQUARED(z)));
	
	return pow(3.0 * (m_vir / (4.0 * PI * density_vir) ), (1.0 / 3.0));

} // end of radius_vir

double mass_vir(double r_vir, double z)
{ // calculates and returns the virial mass (in g) given a virial radius r_vir (in cm) of a halo at redshift z

	double density_vir;

	density_vir = overdensity_vir(z) * RHO_CRIT_0(100. * LITTLE_H * sqrt(E_SQUARED(z)));
	
	return (4.0 * PI / 3.0) * density_vir * pow(r_vir, 3.0);

} // end of mass_vir

double overdensity_vir(double z)
{ /* calculates and returns the overdensity Delta_c with respect to the *total* (rather than matter only) critical density rho_c defining the virial radius of a halo of given virial mass, and which results from
the model of the collapse of a spherical top-hat perturbation */

// Uses equation (6) of Bryan and Norman (1998), ApJ, 495

	double x;
	
	x = (OMEGA_M_0 * (1.0 + z) * (1.0 + z) * (1.0 + z) / E_SQUARED(z)) - 1.0;
	
	return 18.0 * PI * PI + 82.0 * x - 39.0 * x * x;

} // end of overdensity

