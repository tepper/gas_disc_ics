// provides routines the perform a cubic spline interpolation

void dspline(double x[], double y[], int n, double yp1, double ypn, double y2[]);
void dsplint(double xa[], double ya[], double y2a[], int n, double x, double *y);