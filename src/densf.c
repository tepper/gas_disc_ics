// provides various normalised (i.e., dimensionless) density profile functions

#include <math.h>
#include <float.h>
#include "const.h"

// function declarations
double nfw_density_func(double x)
{ /* returns the value of the normalised (i.e., dimensionless) NFW density profile f_rho(x) at a given normalised radius x = r / r_s, where r_s is the scale radius. The physical density is recovered from rho(r) = rho_s * f_rho(x), where rho_s is the (arbitrarily set) scale density */

//	Uses equation (1) of Navarro, Frenk, and White (1997), ApJ 490

	if (x < FLT_MIN) // avoid overflow
		return 1.0 / FLT_MIN;
	else
		return 1.0 / ( (1.0 + x) * (1.0 + x) * x);

} // end of nfw_density_func

double ein_density_func(double x)
{ /* returns the value of the normalised (i.e, dimensionless) Einasto density profile f_rho(x) at a given normalised radius x = r / r_s, where r_s is the scale radius. The physical density is recovered from rho(r) = rho_s * f_rho(x), where rho_s is the (arbitrarily set) scale density

Uses equation (4) of Einasto (1965). Note that the value of the parameter 'alpha' is found to be ~0.18 in DM halos obtained from cosmological simulations (e.g., Gao et al. (2008), Duffy et al. (2008). Following R.S.Sutherland's convention, f_rho(x = 1) = 1 / exp

*/

	double alpha = 0.18;

	return exp( -2.0 * (pow(x, alpha) - 1.0) / alpha ) / EULER;

} // end of ein_density_func

double plu_density_func(double x)
{ // Plummer (1911) spherically symmetric density function; factor 3.0 is required if the corresponding potential is normalised such that phi(0) = 1.0

	return 3.0 / pow((1.0 + x * x), 2.5);


} // end of plu_density_func

double her_density_func(double x)
{ // Hernquist (1990) spherically symmetric density function; need to include a factor M / a^{3} to obtain the actual physical density, where a is the Hernquist scale radius.
// NOTE: the density profile diverges x -> 0 as 1/x, but the mass is bounded.

	double fact = 2. * PI;

	if (x < FLT_MIN) // avoid overflow
		return 1.0 / FLT_MIN;
	else
	return 1.0 / fact / pow((1.0 + x), 3.0) / x;


} // end of plu_density_func

double bur_density_func(double x)
{ // Burkert (1995) spherically symmetric density function

	return 1.0 / (1.0 + x) / (1.0 + x * x);


} // end of plu_density_func

double miy_density_func(double r, double z)
{ // axisymmetric Miyamoto-Nagai (1975, their equation 5) model; density function with variables, R, w^2 = z^2 + b^2, and parameters a and b which correspond to the scale length and scale height, respectively. Note that this is per UNIT MASS

	double a = A_KUZMIN; // kpc; disc scale length from Kafle et al. (2014, their table 2)
	double b = 0.3; // kpc; disc scale height from Kafle et al. (2014, their table 2)
	double w, s;
	double num, denom;
	
	w = sqrt(z * z + b * b);

	s = (a + w);

	num = (b * b) * (a * r * r + (a + 3.0 * w) * s * s);

	denom = 4.0 * PI * pow((r * r + s * s), 2.5) * w * w * w;

	return num / (denom);

} // end of miy_density_func

double kuz_sfdensity_func(double r)
{ // normalised (i.e., dimensionless) Kuzmin (1956) model, a.k.a. Toomre (1963) model 1, describing a razor-thin axisymmetric disc OF UNIT MASS; note that the surface density is zero everywhere except at z = 0, i.e., the plane of the disc; A_KUZMIN is the disc scale length and is defined in const.h

	double a = A_KUZMIN;

	return 1.0 / pow((r * r + a * a), 1.5);

} // end of kuz_sfdensity_func

double exp_sfdensity_func(double x)
{ // Exponential surface density

	return exp(-1.0 * x);

} // end of exp_sfdensity_func

double uni_sfdensity_func(double x)
{ // unity (dummy) surface density

	return 1.0;

} // end of uni_sfdensity_func

