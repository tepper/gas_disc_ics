// Thorsten Tepper Garcia (2016)
/*
REMOVE THE LINE ABOVE AND THIS LINE, AND START THE NEXT LINE WITH slash-star-bang TO INCLUDE THIS DOCUMENTATION IN DOXYGEN

\file  disc_poisson_solver.c

\brief Potential-density pair calculator for 3D, initially isothermal, self-gravitating gaseous discs

This program calculates a self-consistent potential-density pair for a 3D, self-gravitating, isothermal, axisymmetric gas disc with an exponential *surface* density profile in the radial direction (R), in hydrostatic equilibrium in the vertical direction (z), with / without an external potential. For more details, see accompanying \ref documentation.

\attention
<b> IMPORTANT NOTE </b>:
NR routines adopt the "unit-offset" convention, meaning that the lowest subscript of an array is 1 (rather than 0 as in ANSI C, and similar to Fortran). Thus, correct calls to NR-routines which receive arrays are accomplished by offsetting the corresponding argument by -1. Note however that user-supplied functions / routines need to adopt the unit-offset convention as well for the routines to work correctly. Alternatively, and easier to implement, is to declare local arrays with a size larger than actually required by one element, and ignore the first element, corresponding to subscript 0 (in C-programming convention). The latter approach is followed here.

<b> COMPILATION </b>

Invoke the following series of commands from the top directory (Note: you may replace 'cc' by the C-compiler appropriate for your system):

$> rm -f disc_poisson_solver; cd src/.; cc disc_poisson_solver.c nrutil.c rkqs.c rkck.c dodeint.c dspline.c dsplint.c ddfridr.c dbessi0.c dbessi1.c dbessk0.c dbessk1.c cosmo.c densf.c alloc_funcs.c -o disc_poisson_solver; mv disc_poisson_solver ../.; cd ../.

<b> RUNNING </b>

Invoke the executable without further arguments

$> ./disc_poisson_solver

Note: output will be written to STDOUT, so you may want to replace the above
commmand by, e.g. 

$> ./disc_poisson_solver > myICs.dat



\author T.Tepper Garcia (tepper@physics.usyd.edu.au)

\date  Jul, 2016

\todo
	- Allow for binary dump (rather than ascii)
   - Compute (R,z)- range in terms of the actual simulation box size for which the initial conditions are intended, taking into account the scaling by R_d and z_0
   - Allow for a self-consistent solution including an isothermal hot atmosphere in hydrostatic equilibrium with the total potential (perhaps write a separate code to achieve this?)
   - Allow for the use of a logarithmically (rather than linearly) spaced grid
     this is important for a more accurate solution close to the grid's origin
   - Compute the full potential phi(R,z), using equation 33 or 34 of Cuddeford (1993)

<b> Modification history </b>

*/
// --------------------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <math.h>

#include "const.h" // provides numeric / physical constants
#include "densf.h" // provides density functions
#include "nrutil.h"
#include "dodeint.h" // generic PDE integrator
#include "rkqs.h" // 5th order, adaptive step-size RK method
#include "dsplin.h"
#include "ddfridr.h"
#include "dbessi0.h"
#include "dbessi1.h"
#include "dbessk0.h"
#include "dbessk1.h"
#include "alloc_funcs.h"

//-----------------------------------------------------------------------------------------//
// USER DEFINED SETTINGS                                                                   //
// IMPORTANT:                                                                              //
// Be sure to re-compile for the changes to take effect!                                   //

// Include your parameter file here:

// #include "../input_params/disc_exp_hern_Gas0.h"
// #include "../input_params/disc_exp_hern_mn_GasStar0.h"
// #include "../input_params/disc_exp_hern_mn_GasStar0_a.h"
// #include "../input_params/disc_exp_hern_mn_GasStar0_b.h"
// #include "../input_params/disc_exp_hern_mn_GasStar1.h"
// #include "../input_params/disc_exp_SpitzerFreeman_scaleFree.h"
#include "../input_params/disc_exp_plu_mn.h"

//-----------------------------------------------------------------------------------------//


//-----------------------------------------------------------------------------------------//
// NUMERICAL PARAMETERS                                                                    //
// IMPORTANT:                                                                              //
// Change only if you know what you are doing; otherwise it is better to leave as it is    //

#include "../input_params/disc_poisson_solver.h"

//-----------------------------------------------------------------------------------------//


//-----------------------------------------------------------------------------------------//
// DEBUGGING
// uncomment the following flag to dump stuff useful for checking that everything is OK
// comment out when producing actual ICs
// #define OUT_DEBUG
//-----------------------------------------------------------------------------------------//


//-----------------------------------------------------------------------------------------//
// FUNCTION PROTOTYPES
void usage(void);
void poisson_derivs(double x, double w[], double dwdx[]);
double rhoMPFunc(double R); // returns the density f_rho at (sR,x=0); needed to compute v_rot^2_p
double phiGasMPFunc(double R); // returns the potential f_rho at (sR,x=0); needed to compute v_rot^2_g

void innerIntAdsR(double w, double y[], double dydw[]);  // returns the inner integrand in
                                                        // eq. 39 of Cudderford (1993)
void extIntRdA(double theta, double y[], double dydtheta[]);    // returns the exterior integrand

double aParam;  // dummy variable of inner integral in eq. 39 of Cudderford (1993)
double *aCoord = NULL;    // array of values of a
double intA[2]; // value of inner integral at a
double *innerIntA = NULL; // array of values of inner integral at a
double *d2IntAda2 = NULL; // 2nd derivatives wrt a of inner integral at a;
                          // needed for interpolation to compute the exterior integral
double extR[2];           // value of exterior integral at R
double *extIntR = NULL;   // array of values of exterior integral at R

// global variables required by dodeint (do not change this var names!)
long kmax, kount, kmaxSave, kountSave;
double *xp = NULL, **yp = NULL, dxsav;


// User: you may add any desired function here:
double her_potential(double R, double z, double b); // Hernquist (1990) potential
double nfw_potential(double R, double z, double b); // NFW (1996) potential
double plu_potential(double R, double z, double rs); // Plummer (1911) potential
double mn_potential(double R, double z, double a, double b); // Miyamoto-Nagai (1975) potential

double (*phi_dm)(double R, double z, double b) = NULL; // pointer to generic DM (external) potential
double (*phi_dstar)(double R, double z, double a, double b) = NULL; // pointer to generic stellar

double phiExtMPFunc(double sR);        // external potential in the equatorial plane phi(R,0)
double phiExtDiff(double R, double z); // external potential difference phi_z = phi(R,z) - phi(R,0)

double pot_dm_dummy(double R, double z, double b);              // dummy functions (returns 0)
double pot_dstar_dummy(double R, double z, double a, double b); // dummy functions (returns 0)

double (*sfdensity)(double) = NULL;   // pointer to generic surface-density function
double scaledR;               // scaled radial coordinate (default value)
double *W1 = NULL, *d2W1dx2 = NULL; // scaled potential difference and 2nd derivatives
double W1Interp;                      // interpolated scaled potential difference
double *W2 = NULL, *d2W2dx2 = NULL; // scaled centrifugal force and 2nd derivatives
double W2Interp;                      // interpolated scaled potential difference
double *W3 = NULL, *d2W3dx2 = NULL;  // scaled density and second derivatives
double W3Interp;                      // interpolated scaled density
void   W3dx(double x, double y[], double dydx[]);// W_3 dx
double sfDensNorm[2];            // *scaled* surface density normalisation
double *sRCoord = NULL;          // radial coordinate array
double *rhoMidPlane = NULL;      // gas density at x=0; needed to compute v_rot^2_p
double drhoMPdsR;                // and gradient
double *d2rhoMPdsR2 = NULL;      // and 2nd derivatives
double *phiGasMidPlane = NULL;      // gas potential at x=0; needed to compute v_rot^2_g
double phiGasMidPlane0;             // zero-point of gas potential at x=0
double dphiGasMPdsR;                // and gradient
double *d2phiGasMPdsR2 = NULL;      // and 2nd derivatives
float **phiGasDiff = NULL;       // gas potential difference at sR,x
double *phiExtMidPlane = NULL;   // external potential at x=0; needed to compute QToomre
double dphiExtMPdR;              // and gradient
double *d2phiExtMPdsR2 = NULL;   // and 2nd derivatives
double vRot2Prs;                 // *scaled* circular velocity squared ought to pressure support
double vRot2GasPot;              // *scaled* circular velocity squared ought to gravity
double vRot2ExtPot;                 // circular velocity squared ought to external potential
double epicycFreq,epicycFreq2;   // epicyclic frequency
double QToomre;                  // Toomre's parameter
int outsRPoints, outXPoints;   // number of grid points along each coordinate

int main(int argc, char *argv[])
{
   double *sdens_sum = NULL; // z-integrated density, a.k.a. radial surface density
   double sdens_lo, sdens_hi; // used to approximate integral with Simpson's rule
   double phiExt; // total external potential *difference*
   double yCoord; // = R / 2 R_d = sR / 2
   double errGradrhoMP, errGradphiGasMP, errGradphiExtMP, hStep;
   double ScaledVerticalCoord = X_GRID_MIN;
   double *auxPtr = NULL, **auxPptr = NULL;
   float *auxFPtr = NULL, **auxFPptr = NULL;
   void (*derivs)(double, double [], double []) = NULL; // generic derivative function
   double W0, *W = NULL, *dWdx = NULL; // first element of each array will be ignored
   int nok, nbad; // number of good and bad steps taken by dodeint
   int nvar;
   int iterNum, i, j;
   long x_i;

   if (argc != 1) {
      usage();
      return 0;
   }

//-----------------------------------------------------------------------------------------//
// SANITY CHECKS / DEFINITIONS

#ifdef sR_GRID_STEP_LIN
 #ifdef sR_GRID_STEP_LOG
 #undef sR_GRID_STEP_LOG
 #endif
 #define sR_GRID_STEP sR_GRID_STEP_LIN
 #define sR_GRID_PTS (1. + (FLT_EPSILON + (sR_GRID_MAX - sR_GRID_MIN) / (sR_GRID_STEP)))
 #define sR_NEXT_PT(n)  ((n * sR_GRID_STEP) + sR_GRID_MIN)
#endif

#ifdef sR_GRID_STEP_LOG
 #define sR_GRID_STEP sR_GRID_STEP_LOG
 #define sR_GRID_PTS (2. + log10(sR_GRID_MAX - sR_GRID_MIN + 1.) / sR_GRID_STEP)
 #define sR_NEXT_PT(n)  (pow(10., (n * sR_GRID_STEP)) - 1. + sR_GRID_MIN)
#endif

#ifndef SDENS
#define SDENS "none"
#endif

// central surface density <=> gas disc mass
#ifndef SIGMA0
 #ifndef DGAS_MASS
  #define SIGMA0 -1.0
 #else
  #define SIGMA0 (DGAS_MASS * UNIT_MASS/(2.*PI*DGAS_SCALEL*DGAS_SCALEL*UNIT_LENGTH*UNIT_LENGTH))
 #endif
#else
 #ifdef DGAS_MASS
  #undef SIGMA0
  #define SIGMA0 -1.0
 #endif
#endif

// gas disc  isothermal sound speed squared [(cm/s)^2];  scales the gas potential difference and v_rot^2_p
#define DGAS_C_SOUND2 (BOLTZMANN * DGAS_TEMP / (MU * PROTON_MASS))

// scales the gas potential in the equatorial plane and v_rot^2_g
#define PHIGASMP_SCALE (4. * PI * GRAVITY * SIGMA0 * DGAS_SCALEL * UNIT_LENGTH)

// vertical coordinate scale
#define Z0SCALE (DGAS_C_SOUND2 / (2. * PI * GRAVITY * SIGMA0))

// Only relevant if including an external potential
#if (defined(DMTYPE) || defined(SDISCTYPE))
	#ifndef DGAS_SCALEL
	 #define DGAS_SCALEL -1.0
	#endif
	#ifndef DGAS_TEMP
	 #define DGAS_TEMP -1.0
	#endif
	#ifndef MU
	 #define MU -1.0
	#endif
	#ifndef UNIT_LENGTH
	 #define UNIT_LENGTH -1.0
	#endif
#endif // (DMTYPE || SDISCTYPE)

// gas disc with ...

#ifdef DMTYPE  // ... external DM halo potential
 #ifndef DM_HALOMASS
 #define DM_HALOMASS -1.0
 #endif
 #ifndef DM_SCALEL
 #define DM_SCALEL -1.0
 #endif
#else // ... no DM halo potential
 #define DMTYPE "none"
#endif

#ifdef SDISCTYPE  // ... external stellar potential
 #ifndef SDISC_MASS
 #define SDISC_MASS -1.0
 #endif
 #ifndef SDISC_SCALEL
 #define SDISC_SCALEL -1.0
 #endif
 #ifndef SDISC_SCALEL
 #define SDISC_SCALEL -1.0
 #endif
#else // ... no stellar disc potential
 #define SDISCTYPE "none"
#endif

if (strcmp(SDENS, "none") == 0) {
	printf("\nERROR: Need to define a surface density profile (SDENS).\nAbort.\n\n");
	return 0;
}

if(SIGMA0 < 0.0){
	printf("\nERROR: Need to define either a surface density value (SIGMA0)\n");
	printf("       or a total disc gas mass (DGAS_MASS) but not both.\nAbort.\n\n");
	return 0;
}

if(DGAS_SCALEL < 0.0){
	printf("\nERROR: Need to define a gas radial scale length (DGAS_SCALEL).\nAbort.\n\n");
	return 0;
}

if(DGAS_TEMP < 0.0){
	printf("\nERROR: Need to define a gas temperature (DGAS_TEMP).\nAbort.\n\n");
	return 0;
}

if(MU < 0.0){
	printf("\nERROR: Need to define a mean molecular weight (MU).\nAbort.\n\n");
	return 0;
}

if(UNIT_LENGTH < 0.0){
	printf("\nERROR: Need to define a length unit (UNIT_LENGTH).\nAbort.\n\n");
	return 0;
}

if(DM_HALOMASS < 0.0){
	printf("\nERROR: Need to define a DM halo mass (DM_SCALEL).\nAbort.\n\n");
	return 0;
}

if(DM_SCALEL < 0.0){
	printf("\nERROR: Need to define a DM scale length (DM_SCALEL).\nAbort.\n\n");
	return 0;
}

if(SDISC_MASS < 0.0){
	printf("\nERROR: Need to define a stellar disc's mass (SDISC_MASS).\nAbort.\n\n");
	return 0;
}
if(SDISC_SCALEL < 0.0){
	printf("\nERROR: Need to define a stellar disc's scale length (SDISC_SCALEL).\nAbort.\n\n");
	return 0;
}
if(SDISC_SCALEH < 0.0){
	printf("\nERROR: Need to define a stellar disc's scale height (SDISC_SCALEH).\nAbort.\n\n");
	return 0;
}

if(X_GRID_MIN < 0.0){
	printf("\nERROR: Need X_GRID_MIN = 0.0 to to compute density gradient at midplane!!\nAbort.\n\n");
	return 0;
}
if(sR_GRID_MAX == sR_GRID_MIN){
	printf("\nERROR: Not enough grid points along R to compute density gradient!\nAbort.\n\n");
	return 0;
}

//-----------------------------------------------------------------------------------------//

// TESTING

// 	printf("\n%.8E\n", DGAS_MASS);
// 	printf("\n%.8E\n", SIGMA0);
// 	printf("\n%.8E\n", DGAS_SCALEL);
// 	printf("\n%.8E\n", MU);
// 	printf("\n%.8E\n", DGAS_TEMP);
// 	printf("\n%.8E\n", PHIGASMP_SCALE);
// 
// 	return 0;

//-----------------------------------------------------------------------------------------//
// SET-UP: choose appropriate settings depending on the system chosen (i.e., uni, exp, ...)

// here a new block may be added to solve for any surface-density function
// note that the corresponding function needs to be defined in densf.[h,c]

   // constant (unity) surface density: rho_R(R) = 1.
   if (strcmp(SDENS, "uni") == 0) {
      sfdensity = uni_sfdensity_func; // surface density function rho_R(R)
   } // end of uni

   // normalised exponential surface density: f_rho(R) = exp( -R / R_d )
   if (strcmp(SDENS, "exp") == 0) {
      sfdensity = exp_sfdensity_func; // surface density function rho_R(R)
   } // end of exp

   // allocate scaled variables and their first derivatives
   // recall unit-offset; first element will be ignored
   nvar = 3;
   W = (double *) alloc1DArr(sizeof(double), (nvar + 1));
   dWdx = (double *) alloc1DArr(sizeof(double), (nvar + 1));

   // set external potential function
   phi_dm = pot_dm_dummy;
   phi_dstar = pot_dstar_dummy;
   if (strcmp(DMTYPE, "her") == 0)
      phi_dm = her_potential;
   if (strcmp(DMTYPE, "nfw") == 0)
      phi_dm = nfw_potential;
   if (strcmp(DMTYPE, "plu") == 0)
      phi_dm = plu_potential;
   if (strcmp(SDISCTYPE, "mn") == 0)
      phi_dstar = mn_potential;

   // define number of output values; grid will have size outsRPoints x outXPoints

		// LINEAR GRID
		// avoid round-off errors by adding FLT_EPSILON
		outsRPoints = 1 + (FLT_EPSILON + (sR_GRID_MAX - sR_GRID_MIN) / (sR_GRID_STEP));
		outXPoints = 1 + (FLT_EPSILON + (X_GRID_MAX - X_GRID_MIN) / (X_GRID_STEP));

		// LOGARITHMIC GRID (experimental)
// 		outsRPoints = (int) sR_GRID_PTS;
// 		outXPoints = 1 + (FLT_EPSILON + (X_GRID_MAX - X_GRID_MIN) / (X_GRID_STEP));

// TESTING
// 	printf("\n%d %d\n", outsRPoints, outXPoints);
//    scaledR = sR_GRID_MIN;
//    for(i = 1; i <= outsRPoints; i++){
//    	printf("%d %f\n", i, scaledR);
//   	   scaledR = sR_NEXT_PT(i);
// 	}
// 	return 0;

   // allocate radial coordinate array
   sRCoord = (double *) alloc1DArr(sizeof(double), (outsRPoints + 1));

   // allocate gas density | gas potential in the equatorial plane and derivatives (1st and 2nd)
   rhoMidPlane = (double *) alloc1DArr(sizeof(double), (outsRPoints + 1));
   d2rhoMPdsR2 = (double *) alloc1DArr(sizeof(double), (outsRPoints + 1));

   // allocate external potential in the equatorial plane and 2nd derivatives
   phiExtMidPlane = (double *) alloc1DArr(sizeof(double), (outsRPoints + 1));
   d2phiExtMPdsR2 = (double *) alloc1DArr(sizeof(double), (outsRPoints + 1));

   // allocate radial surface density
   sdens_sum = (double *) alloc1DArr(sizeof(double), (outsRPoints + 1));

// ODE integrator set-up (see dodeint.c)

   // set output stepsize (approx.) and maximum number of steps between X_INT_ORIGIN and X_INT_INFTY

   dxsav = X_INT_STEP_SAVE; // values are saved approximately at this interval
   kmax = ceil( (X_INT_INFTY - X_INT_ORIGIN) / dxsav); // if kmax = 0 -> no intermediate results saved
   kmaxSave = kmax;

   // allocate arrays to store intermediate results;
   // recall unit-offset; first element will be ignored
   // IMPORTANT: DO NOT TRY ALLOCATING yp AS CONTIGUOUS CHUNKS, I.E.
   //
   // yp = (double **) alloc2DArr(sizeof(double), (nvar + 1), (kmax + 1));
   //
   // SINCE IT WILL GENERALLY FAIL FOR LARGE kmax

   // allocate an array of size (kmax + 1) and type double
   xp = (double *) alloc1DArr(sizeof(double), (kmax + 1));

   // allocate a (nvar + 1) rows of pointers to double
   if ( (auxPptr = (double **) malloc( (nvar + 1) * sizeof(double *))) != NULL) {
      yp = auxPptr;
      // allocate an array of size (kmax + 1) and type double
      for (j = 0; j <= nvar; j++){
         if ( (auxPtr = malloc( (kmax + 1) * sizeof(double))) != NULL )
            *(yp + j) = auxPtr;
         else {
            printf("failed to allocate memory for object '*yp'\n");
            return 0;
         }
         auxPtr = NULL;
      }
   }
   else {
      printf("failed to allocate memory for object 'yp'\n");
      return 0;
   }

   // allocate memory for potential | force | density and derivatives
   // needed to interpolate potential / density
   // recall unit-offset; first element will be ignored
   W1 = (double *) alloc1DArr(sizeof(double), (kmax + 1));
   d2W1dx2 = (double *) alloc1DArr(sizeof(double), (kmax + 1));
   W2 = (double *) alloc1DArr(sizeof(double), (kmax + 1));
   d2W2dx2 = (double *) alloc1DArr(sizeof(double), (kmax + 1));
   W3 = (double *) alloc1DArr(sizeof(double), (kmax + 1));
   d2W3dx2 = (double *) alloc1DArr(sizeof(double), (kmax + 1));


   // allocate memory for total gas potential
   if ( (auxFPptr = malloc( (outsRPoints + 1) * sizeof(float *))) != NULL) {
      phiGasDiff = auxFPptr;
      for (j = 0; j <= outsRPoints; j++){
         if ( (auxFPtr = malloc( (outXPoints + 1) * sizeof(float))) != NULL )
            *(phiGasDiff + j) = auxFPtr;
         else {
            printf("failed to allocate memory for object '*phiGasDiff'\n");
            return 0;
         }
         auxFPtr = NULL;
      }
   }
   else {
      printf("failed to allocate memory for object 'phiGasDiff'\n");
      return 0;
   }


// INTEGRATION LOOP (OVER R { OVER guesss for rho_R(R) until convergence (defined how?) } )

// NOTE: output is written to stdout on-the-fly

#ifndef OUT_DEBUG
   // the following line provides the size of the grid along each dimension
   printf("# Rows Cols  Unit_L[cm]  Unit_M[g]  SDProf  Sigma0     Rd   Mu    Temp[K]    DMpot  DMmass     DMscalel   SDISCpot SDISCmass    SDISCscalel SDISCscaleh\n");
   printf("#%5d%5d  %.3lE   %.3lE  %s     %.3lE  %.2f %.2f  %.3lE  %4s   %.3lE  %.3lE  %4s     %.3lE    %.3lE   %.3lE\n",
      outsRPoints, outXPoints,
      UNIT_LENGTH,
      UNIT_MASS,
      SDENS, SIGMA0, DGAS_SCALEL, MU, DGAS_TEMP,
      DMTYPE, DM_HALOMASS, DM_SCALEL,
      SDISCTYPE, SDISC_MASS, SDISC_SCALEL, SDISC_SCALEH);
   printf("#  sR \\ x   \n"); // grid arrangement

   // the following line provides each of the value of the (scaled) vertical
   // coordinate (2 to numPoints-th column)
   printf("%5d     ", outXPoints);   // this is useful when plotting the result with gnuplot
                                     // using nonuniform matrix
   for(j = 0; j < outXPoints; j++){
      printf("%.8lE ", ScaledVerticalCoord);
      ScaledVerticalCoord += X_GRID_STEP;
   }
   printf("\n"); // line break
#else
		printf("# z-points      sR      integral S     iterations\n");
#endif

   // reset initial value of vertical coordinate
   ScaledVerticalCoord = X_GRID_MIN;

   // HERE: start loop over radial coordinate R
   scaledR = sR_GRID_MIN;

   for(i = 1; i <= outsRPoints; i++){ // loop over number of sR-grid points

      // HERE: loop until convergence

      sfDensNorm[1] = 1.0; // first guess for surface density normalisation implies rho_R(R) = exp(-R/R_d)
      sfDensNorm[0] = 0.0; // initial value to start iteration

      iterNum = 0;
      while(fabs(sfDensNorm[1]-sfDensNorm[0])/sfDensNorm[1] >= CONVERGENCE){

         if(iterNum < ITER_MAX){

            iterNum++;

            sfDensNorm[0] = sfDensNorm[1]; // save initial value

            // 1) integrate Poisson's equation for a given R, an initial guess for rho_R(R)
            // from X_INT_ORIGIN to X_INT_INFTY, and save result in roughly dxsav steps
            // NOTE: yp[1][x_i] = W_1[x_i]; yp[3][x_i] = W_3[x_i] = exp[ W_1[x_i] ]

            // (re)set initial values of scaled variables and their derivatives at X_INT_ORIGIN
               kmax = kmaxSave;
               W0 = 0.0;         // potential zero-point
               W[1] = W0;        // potential difference
               W[2] = 0.0;       // force
               W[3] = 1.0;       // density (with initial guess SS = 1)
               derivs = poisson_derivs;   // derivatives dW1/dx, etc. at any x

            // actually integrate
               dodeint(W, nvar, X_INT_ORIGIN, X_INT_INFTY, PRECISION, X_INT_STEP, X_INT_STEP_MIN,
                  &nok, &nbad, derivs, rkqs);

            // set-up integration of surface density normalisation
               // store potential | force | density and 2nd derivatives
               // needed for interpolation of integrand exp[W1])
               for (x_i = 1; x_i <= kount; x_i++){
                  W1[x_i] = yp[1][x_i];
               }
               dspline(xp, W1, kount, 1.0E30, 1.0E30, d2W1dx2);

               for (x_i = 1; x_i <= kount; x_i++){
                  W2[x_i] = yp[2][x_i];
               }
               dspline(xp, W2, kount, 1.0E30, 1.0E30, d2W2dx2);

               for (x_i = 1; x_i <= kount; x_i++){
                  W3[x_i] = yp[3][x_i];
               }
               dspline(xp, W3, kount, 1.0E30, 1.0E30, d2W3dx2);

            // Compute *scaled* surface density normalisation at R
               kountSave = kount;// save size of arrays from integration above
               kmax = 0;         // do not save intermediate results of the next integration
               sfDensNorm[1] = 0.0; // boundary condition i.e. value of integral at z = 0
               derivs = W3dx;       // define integrand

               // recall: integral from x = 0 to +infty
               dodeint(sfDensNorm, 1, X_INT_ORIGIN, X_INT_INFTY, PRECISION, X_INT_STEP, X_INT_STEP_MIN,
                &nok, &nbad, derivs, rkqs);

         } else {

            printf("\nWARNING: Maximum number of iterations reached (without convergence)!");         
            printf("\n         Increase ITER_MAX or increase CONVERGENCE.\n");
            sfDensNorm[1] = sfDensNorm[0];
            i += outsRPoints;

         }

      } // END: loop until convergence

      // reset value of vertical coordinate
      ScaledVerticalCoord = X_GRID_MIN;

      // DEBUGGING OUTPUT

#ifdef OUT_DEBUG
      printf("%4d   %14.3lE  %12.6lE %8d\n", outXPoints, scaledR, sfDensNorm[1], iterNum);
//       for(j = 1; j <= outXPoints; j++){
//       // interpolate density | force | potential difference at ScaledVerticalCoord
//          dsplint(xp, W3, d2W3dx2, kountSave, ScaledVerticalCoord, &W3Interp);
//          dsplint(xp, W2, d2W2dx2, kountSave, ScaledVerticalCoord, &W2Interp);
//          dsplint(xp, W1, d2W1dx2, kountSave, ScaledVerticalCoord, &W1Interp);
//          printf("%.3lE %.3lE %.8lE %.8lE %.8lE %.8lE %.8lE\n",
//          scaledR,
//          ScaledVerticalCoord,
//          W3Interp,
//          W1Interp,
//          W2Interp,
//          sfdensity(scaledR),
//          sfDensNorm[1]);
//          ScaledVerticalCoord += X_GRID_STEP;
//       }
//       printf("\n\n");
#endif

      // ACTUAL OUTPUT

#ifndef OUT_DEBUG // dump density on (sR,x) grid to stdout; compute surface density at sR

		// set z-integrated density
   	sdens_sum[i] = 0.;
   	sdens_lo = 0.;
   	sdens_hi = 0.;

      // the following line provides the value of sR (1st column),
      // and the value of the scaled gas density at each x; 2 to numPoints-th column)
      printf("%.3lE ", scaledR);
      for(j = 1; j <= outXPoints; j++){

      // interpolate gas density at ScaledVerticalCoord
      // NOT recommended because interpolation around very small values may lead to
      // negative densities
//          dsplint(xp, W3, d2W3dx2, kountSave, ScaledVerticalCoord, &W3Interp);
      //instead:

      // interpolate gas potential difference at ScaledVerticalCoord
         dsplint(xp, W1, d2W1dx2, kountSave, ScaledVerticalCoord, &W1Interp);

      // calculate external potential at sR,x
      // Note: phiExt = 0 unless a DM halo or stellar disc potential(s) are included
         phiExt = phiExtDiff(scaledR, ScaledVerticalCoord);

		// dump scaled gas density to stdout
//          printf("%.8lE ",
//          (sfdensity(scaledR) / sfDensNorm[1]) * W3Interp * exp(phiExt));
//			double precision
//          printf("%.8lE ",
//          (sfdensity(scaledR) / sfDensNorm[1]) * exp(W1Interp) * exp(phiExt));
//			single precision
         printf("%.8E ",
         (float) ((sfdensity(scaledR) / sfDensNorm[1]) * exp(W1Interp) * exp(phiExt)) );

		// compute surface density (simple mid-point integration)
         sdens_lo = sdens_hi;
//          sdens_hi = (sfdensity(scaledR) / sfDensNorm[1]) * W3Interp * exp(phiExt);
         sdens_hi = (sfdensity(scaledR) / sfDensNorm[1]) * exp(W1Interp) * exp(phiExt);
   		if(j>1)sdens_sum[i] +=  0.5 * (sdens_lo + sdens_hi) * X_GRID_STEP;

		// store gas potential-difference at sR,x
		//	NOTE: SHOULD BE WITH FACTOR -1 OR NOT? I GUESS IT DEPENDS ON THE CHOICE OF 0-POINT...
// 			phiGasDiff[i][j] = -1.0 * W1Interp;
			phiGasDiff[i][j] = W1Interp;

		// increase vertical coordinate
         ScaledVerticalCoord += X_GRID_STEP;

      }
      printf("\n"); // line break
#endif

      // save value of radial coordinate and gas density and external potential
      // in the equatorial plane (x=0) for current sR
      sRCoord[i] = scaledR;
      rhoMidPlane[i] = (sfdensity(scaledR) / sfDensNorm[1]);
		phiExtMidPlane[i] = phiExtMPFunc(scaledR);

      // increase value of radial coordinate
      scaledR += sR_GRID_STEP;

   } // END: for-i-loop over sR

//-----------------------------------------------------------------------------------------//
// Compute potential due to gas in the equatorial plane, phi_g(sR,0)
// Depends ONLY on the functional form of the surface density
// See Cuddeford (1993; their eq. 39)

   // 1: Interior integral 

      // allocate arrays
      aCoord = (double *) alloc1DArr(sizeof(double), ((A_GRID_PTS) + 1));
      innerIntA = (double *) alloc1DArr(sizeof(double), ((A_GRID_PTS) + 1));

      // set up integration parameters
      kmax = 0;              // do not save intermediate results
      derivs = innerIntAdsR; // define integrand
      aParam = A_MIN;

      // HERE: loop over a
      // compute and save values of interior integral for each value of a
      for(i = 1; i <= (A_GRID_PTS); i++){

         intA[1] = 0.0;      // boundary condition i.e. value of integral at sR = a

         // integral from sR = a to +infty; using w defined by sR^2 = w^2 + a^2
         // i.e., integral effectively from 0 to +infty
         dodeint(intA, 1, X_INT_ORIGIN, X_INT_INFTY, PRECISION, X_INT_STEP, X_INT_STEP_MIN,
          &nok, &nbad, derivs, rkqs);

         aCoord[i] = aParam;     // save value of a
         innerIntA[i] = intA[1]; // save value of integral at a

#ifdef OUT_DEBUG
//          printf("%.3lE %.8lE\n", aCoord[i], innerIntA[i]);
#endif
         aParam += A_STEP; // increase a-value

      } // END: for-i loop over a

#ifdef OUT_DEBUG
//       printf("\n"); // line break
#endif

   // 2: Exterior integral -> potential due to gas in the equatorial plane

      phiGasMidPlane = (double *) alloc1DArr(sizeof(double), (outsRPoints + 1));
      d2phiGasMPdsR2 = (double *) alloc1DArr(sizeof(double), (outsRPoints + 1));

      // compute 2nd derivatives with respect to a of interior integral;
      // needed for interpolation to compute the exterior integral
      // allocate arrays
      d2IntAda2 = (double *) alloc1DArr(sizeof(double), ((A_GRID_PTS) + 1));

      dspline(aCoord, innerIntA, ((A_GRID_PTS) + 1), 1.0E30, 1.0E30, d2IntAda2);

      // set up integration parameters
      kmax = 0;              // do not save intermediate results
      derivs = extIntRdA;    // define integrand

      // HERE: loop over sR
      // compute values of exterior integral for each value of sR (see Cuddeford 1993; eq. 39)
      for(j = 1; j <= outsRPoints; j++){

         scaledR = sRCoord[j];

         extR[1] = 0.0;      // boundary condition i.e. value of integral at x = 0

         // integral from a = 0 to sR; using theta defined by a = sR * sin(theta pi/2)
         // i.e., integral effectively from 0 to 1
         dodeint(extR, 1, X_INT_ORIGIN, 1.0, PRECISION, X_INT_STEP, X_INT_STEP_MIN,
          &nok, &nbad, derivs, rkqs);

         phiGasMidPlane[j] = extR[1];

#ifdef OUT_DEBUG
//          printf("%.3lE %.8lE\n", scaledR, phiGasMidPlane[j]);
#endif

      } // END: for j-loop over sR

      // zero-point of potential in the equatorial plane
      phiGasMidPlane0 = phiGasMidPlane[1];

// END of computation of potential due to gas in the equatorial plane, phi_g(sR,0)
//-----------------------------------------------------------------------------------------//


//-----------------------------------------------------------------------------------------//
// dump value to stdout of:
// - *scaled* v_rot^2_p = (sR/f_rho) df_rho/dsR on sR-grid at x=0
// - *scaled* v_rot^2_g = (sR) dphi_gas/dsR on sR-grid at x=0
// - *physical" v_rot^2_pot = (sR) dphi_ext/dsR on sR-grid at x=0
// - *scaled* f_rho on sR-grid at x=0
// - *scaled* phi_gas on sR-grid at x=0
// - *scaled* gas surface density on sR-grid
// - Toomre's Q parameter on sR-grid
#ifndef OUT_DEBUG

   printf("\n\n#z-points   sR    integral S\n");

   // compute second derivatives of f_rho(sR,x=0) and phi_g(sR,x=0) and phi_ext(sR,x=0)
   dspline(sRCoord, rhoMidPlane, outsRPoints, 1.0E30, 1.0E30, d2rhoMPdsR2);
   dspline(sRCoord, phiGasMidPlane, outsRPoints, 1.0E30, 1.0E30, d2phiGasMPdsR2);
   dspline(sRCoord, phiExtMidPlane, outsRPoints, 1.0E30, 1.0E30, d2phiExtMPdsR2);

   // compute density | potential sR-gradient
   errGradrhoMP = 0.0;
   errGradphiGasMP = 0.0;
   errGradphiExtMP = 0.0;
   hStep = (2*sR_GRID_STEP); // step chosen following the NR recommendation
 	epicycFreq2 = 0.0;
 	epicycFreq = 0.0;



   for(j = 1; j <= outsRPoints; j++){

      scaledR = sRCoord[j];

      drhoMPdsR    = ddfridr(rhoMPFunc, scaledR, hStep, &errGradrhoMP);
      dphiGasMPdsR = ddfridr(phiGasMPFunc, scaledR, hStep, &errGradphiGasMP);
      dphiExtMPdR  = ddfridr(phiExtMPFunc, scaledR, hStep, &errGradphiExtMP);

      // Calculate each contribution to the azimuthal velocity
      vRot2Prs    = (scaledR/rhoMidPlane[j]) * drhoMPdsR;  // scaled v_rot^2_p
      vRot2GasPot = scaledR * dphiGasMPdsR;                // scaled v_rot^2_g
      vRot2ExtPot = scaledR * dphiExtMPdR;                 // physical v_rot^2_phi_ext

// NOTE: analytic solution for v_rot^2_g; can be used to test accuracy of phi_g(sR,x=0)
//       yCoord = 0.5 * scaledR;
//       vRot2GasPot = yCoord * yCoord *
//                   (dbessi0(yCoord) * dbessk0(yCoord) - dbessi1(yCoord) * dbessk1(yCoord));

      // Calculate Toomre's Q parameter
      // epicyclic frequency squared; note that the physical potential in the equatorial plane is recovered
      // by scaling the quantity phiGasMidPlane by PHIGASMP_SCALE = 4 Pi G Sigma_0 R_d (see also below).
 		if(scaledR > 0.0)
 			epicycFreq2 = ((3.0 / scaledR) * ((PHIGASMP_SCALE) * dphiGasMPdsR + dphiExtMPdR)
 			            + ((PHIGASMP_SCALE) * d2phiGasMPdsR2[j] + d2phiExtMPdsR2[j]));
 		//actual epicyclic frequency; take R-coordinate scaling into account
		epicycFreq = sqrt(epicycFreq2 / (UNIT_LENGTH * DGAS_SCALEL) / (UNIT_LENGTH * DGAS_SCALEL));
		// actual Q parameter
		QToomre = sqrt(DGAS_C_SOUND2) * epicycFreq / ( PI * GRAVITY * (SIGMA0 * sfdensity(scaledR)) );

		// output to stdout
      printf("%.3lE %.8lE %.8lE %.8lE %.8lE %.8lE %.8lE %.8lE %.8lE %.8lE %.8lE\n",
      scaledR,                               // radial coordinate
      vRot2Prs,                              // scaled v_rot^2_p
      vRot2GasPot,                           // scaled v_rot^2_g
      vRot2ExtPot,                           // physical v_rot^2_phi
      rhoMidPlane[j],                        // f_rho(sR,x=0)
      phiGasMidPlane0 - phiGasMidPlane[j],   // scaled phi_g(sR,x=0) with phi_g(0,0) = 0 and phi_g < 0
      errGradrhoMP,                          // error in df_rho/dsR(z=0)
      errGradphiGasMP,                       // error in scaled dphi_g/dsR(z=0)
      errGradphiExtMP,                       // error in dphiExt/dsR(z=0)
      sdens_sum[j],                          // surface density
      QToomre);                              // Toomre's parameter

   }

// Output total, approximated gas potential obtained from (see intro):
//
//			Phi(R,z) = Phi_z(R,z) + Phi(R,0)
//
// IMPORTANT: note that each term is scaled in a different way! Indeed, the physical potential
// in the equatorial plane, Phi(R,0), is recovered by scaling the quantity phiGasMidPlane by 4 Pi G Sigma_0 R_d,
// whereas the potential difference in physical units, Phi_z(R,z), is recovered by scaling W_1 by cs^2.

//    printf("\n\n#  sR \\ x   [gas potential difference in scaled units] \n"); // grid arrangement
   printf("\n\n#  sR \\ x   [approximated gas potential difference in scaled units] \n"); // grid arrangement
   printf("%5d     ", outXPoints);

   // reset value of vertical coordinate
   ScaledVerticalCoord = X_GRID_MIN;
   for(j = 0; j < outXPoints; j++){
      printf("%.8lE ", ScaledVerticalCoord);
      ScaledVerticalCoord += X_GRID_STEP;
   }
   printf("\n"); // line break
   for(i = 1; i <= outsRPoints; i++){ // loop over number of sR-grid points
      	printf("%.3lE ", sRCoord[i]);
      for(j = 1; j <= outXPoints; j++)
      	printf("%.8E ", DGAS_C_SOUND2 * phiGasDiff[i][j] + PHIGASMP_SCALE * (float) (phiGasMidPlane0 - phiGasMidPlane[i]) );
      printf("\n");
	}
#else // debugging / testing output

   printf("\n\n#  sR      v_rot^2(num)     v_rot^2(ana) \n");

   // compute second derivatives of f_rho(sR,x=0) and phi_g(sR,x=0) and phi_ext(sR,x=0)
   dspline(sRCoord, rhoMidPlane, outsRPoints, 1.0E30, 1.0E30, d2rhoMPdsR2);
   dspline(sRCoord, phiGasMidPlane, outsRPoints, 1.0E30, 1.0E30, d2phiGasMPdsR2);
   dspline(sRCoord, phiExtMidPlane, outsRPoints, 1.0E30, 1.0E30, d2phiExtMPdsR2);

   // compute density | potential sR-gradient
   errGradrhoMP = 0.0;
   errGradphiGasMP = 0.0;
   errGradphiExtMP = 0.0;
   hStep = (2*sR_GRID_STEP); // step chosen following the NR recommendation
 	epicycFreq2 = 0.0;
 	epicycFreq = 0.0;



   for(j = 1; j <= outsRPoints; j++){

      scaledR = sRCoord[j];
      dphiGasMPdsR = ddfridr(phiGasMPFunc, scaledR, hStep, &errGradphiGasMP);

		// numeric solution for v_rot^2_g
      vRot2GasPot = scaledR * dphiGasMPdsR;

      printf("%.3lE %.8lE",
      scaledR,                               // radial coordinate
      vRot2GasPot);                          // numerical scaled v_rot^2_g

		// analytic solution for v_rot^2_g; Bessel functions undefined at 0
      yCoord = (0.5 * scaledR > 0. ? 0.5 * scaledR : 1.0E-6);
      vRot2GasPot = yCoord * yCoord *
                  (dbessi0(yCoord) * dbessk0(yCoord) - dbessi1(yCoord) * dbessk1(yCoord));


		// output to stdout
      printf(" %.8lE\n",
      vRot2GasPot);                          // analytic scaled v_rot^2_g

   }

#endif
// END of dump value to stdout
//-----------------------------------------------------------------------------------------//

   return 0;

} // end of main
//-----------------------------------------------------------------------------------------//



//-----------------------------------------------------------------------------------------//
// FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------------------//


//-----------------------------------------------------------------------------------------//
// EXTERNAL POTENTIALS
// User: add here any desired function

double her_potential(double R, double z, double b)
{ // Hernquist (1990) potential

   double r;
   
   r = sqrt(R*R + z*z);

	return -1.0 / (r + b); 

} // end of her_potential

double nfw_potential(double R, double z, double b)
{ // NFW (1996) potential

   double r, phi;
   
   r = sqrt(R*R + z*z);

  // limit when r -> 0
	phi = (r > 1.0E-16 ? -1.0 * (log(1.0 + r/b) / r) : -1.0);
	
	return phi;

} // end of nfw_potential

double plu_potential(double R, double z, double rs)
{ // Plummer (1911) scaled potential

   double r2,rs2;

	R = (R > 0. ? R : 0.);
	z = (z > 0. ? z : 0.);

   r2		= (R*R + z*z);
   rs2	= rs*rs;

	return -1.0 / sqrt(r2 + rs2); 

} // end of plu_potential

double mn_potential(double R, double z, double a, double b)
{ // Miyamoto-Nagai (1975) potential

	double w, s;
	
	w = sqrt(z*z + b*b);

	s = (a + w);

	return -1.0 / sqrt(R*R + s*s);

} // end of mn_potential

//-----------------------------------------------------------------------------------------//


//-----------------------------------------------------------------------------------------//

// POISSON EQUATION
void poisson_derivs(double x, double w[], double dwdx[])
{ //  Provides dW_i/dz, i=1,2,3 at x

   double phi_ext; // total external potential *difference*

   phi_ext = phiExtDiff(scaledR, x);

   // system of differential equations to solve:
   dwdx[1] = w[2];
   dwdx[2] = -1.0 * w[3] * (sfdensity(scaledR) / sfDensNorm[1]) * exp(phi_ext);
   dwdx[3] = w[2] * w[3];

   return;

} // end of poisson_derivs

// External potential difference


// INTEGRAND OF SURFACE DENSITY NORMALISATION
void W3dx(double x, double y[], double dydx[])
{ // returns scaled vertical density W3 = exp[W1] * exp[ - Phi_z / cs^2 ]

   double intRho,intPhi;
   double phi_ext; // total external potential *difference*

   // interpolate W3 = exp(W1) at x
   // NOT recommended because interpolation around very small values may lead to
   // negative densities
//    dsplint(xp, W3, d2W3dx2, kountSave, x, &intRho);
   //instead:

   // interpolate W1 at x
   dsplint(xp, W1, d2W1dx2, kountSave, x, &intPhi);
	intRho = exp(intPhi);

   // external potential difference
   phi_ext = phiExtDiff(scaledR, x);

   dydx[1] = (intRho > 0.0 ? intRho * exp(phi_ext) : 0.0);

   return;

} // end of W3dx


// EXTERNAL POTENTIAL IN THE EQUATORIAL PLANE
double phiExtMPFunc(double sR)
{
   double R, phi_ext;

   R = UNIT_LENGTH * DGAS_SCALEL * sR;

   // DM halo's contribution
   phi_ext  = phi_dm(R,0.,(DM_SCALEL*UNIT_LENGTH)) * (GRAVITY * (DM_HALOMASS*UNIT_MASS));

   // stellar disc's contribution
   phi_ext += phi_dstar(R,0.,(SDISC_SCALEL*UNIT_LENGTH),(SDISC_SCALEH*UNIT_LENGTH)) * (GRAVITY * (SDISC_MASS*UNIT_MASS));

   return phi_ext;

} // end of phiExtMPFunc

// EXTERNAL POTENTIAL (DM halo and/or stellar potential) DIFFERENCE AT R,z
double phiExtDiff(double R, double z)
{
   double phi_ext;

   R = UNIT_LENGTH * DGAS_SCALEL * R;
   z = Z0SCALE * z;

   // DM halo's contribution
   phi_ext  = (phi_dm(R,z,(DM_SCALEL*UNIT_LENGTH)) - phi_dm(R,0.,(DM_SCALEL*UNIT_LENGTH))) * (GRAVITY * (DM_HALOMASS*UNIT_MASS));

   // stellar disc's contribution
   phi_ext += ( phi_dstar(R,z,(SDISC_SCALEL*UNIT_LENGTH),(SDISC_SCALEH*UNIT_LENGTH)) - 
               phi_dstar(R,0.,(SDISC_SCALEL*UNIT_LENGTH),(SDISC_SCALEH*UNIT_LENGTH)) ) * (GRAVITY * (SDISC_MASS*UNIT_MASS));

   phi_ext = -1.0 * phi_ext / DGAS_C_SOUND2;

   return phi_ext;

} // end of phiExtDiff


// VOLUME DENSITY IN THE EQUATORIAL PLANE
double rhoMPFunc(double sR)
{// returns the density f_rho(sR,x=0); needed to compute df_rho/dsR at sR and x=0

   double intRhoMP;

   dsplint(sRCoord, rhoMidPlane, d2rhoMPdsR2, outsRPoints, sR, &intRhoMP);

   return intRhoMP;

} // end of rhoMPFunc


// GAS POTENTIAL IN THE EQUATORIAL PLANE
double phiGasMPFunc(double sR)
{// returns the potential phi_g(sR,x=0); needed to compute dphi_g/dsR at sR and x=0

   double intphiGasMP;

   dsplint(sRCoord, phiGasMidPlane, d2phiGasMPdsR2, outsRPoints, sR, &intphiGasMP);

   return intphiGasMP;

} // end of phiGasMPFunc


// INNER INTEGRAL TO CALCULATE GAS POTENTIAL IN THE EQUATORIAL PLANE
void innerIntAdsR(double w, double y[], double dydw[])
{ // provides the integrand of the interior derivative to compute
  // the potential due to the surface gas density see eq. 39 of
  // Cuddeford 1993). Note that the actual integration variable
  // in the original expression R' is replaced by w^2 = R'^2 - a^2
  // to avoid the singularity of the integrand at R' = a

   double x;

   x = sqrt(w*w + aParam*aParam);

   dydw[1] = sfdensity(x);

   return;

} // end of innerIntAdsR

// EXTERIOR INTEGRAL TO CALCULATE GAS POTENTIAL IN THE EQUATORIAL PLANE
void extIntRdA(double theta, double y[], double dydtheta[])
{ // provides the integrand of the exterior derivative to compute
  // the potential due to the surface gas density see eq. 39 of
  // Cuddeford 1993). Note that the actual integration variable a
  // is replaced by a = R sin(theta pi/2) to avoid the singularity
  // of the integrand at a = R. Here, of course, R is also replaced by sR.
  // Note also that the constant factor preceding the integral given
  // below is actually -4 G * (pi / 2), taking into account the
  // change of variable (in the original expression it is -4 G),
  // but since we are using units in which 4 pi G = 1 throughout,
  // the factor becomes simply -1/2.

   double interpIntA, x;

   x = scaledR * sin(0.5*PI*theta);

   dsplint(aCoord, innerIntA, d2IntAda2, A_GRID_PTS, x, &interpIntA);

   dydtheta[1] = -0.5 * interpIntA;

   return;

} // end of extIntRdA


// USAGE
void usage(void)
{
   printf("\nUSAGE:\n\n./disc_poisson_solver\n\n");
   printf("Editable options can be found in the source code's header.\n");
   printf("Be sure to re-compile for the changes to take effect!\n\n");


} // end of usage
//-----------------------------------------------------------------------------------------//


//-----------------------------------------------------------------------------------------//
// DUMMY FUNCTIONS

double pot_dm_dummy(double R, double z, double b)
{ // dummy DM potential function

	return 0.0;

} // end of pot_dm_dummy


double pot_dstar_dummy(double R, double z, double a, double b)
{ // dummy stellar potential function

	return 0.0;

} // end of pot_dstar_dummy

//-----------------------------------------------------------------------------------------//
