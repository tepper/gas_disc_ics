// provide various definitions (parameters, functions) of cosmological quantities

// symbolic constants and macros
#define LITTLE_H 0.7
#define OMEGA_M_0 0.3
#define OMEGA_L_0 (1.0 - OMEGA_M_0)
#define RHO_CRIT_0(H) (3.0E-2 * ( (H) * (H) / CM_PER_PC / CM_PER_PC) / (8.0 * PI * GRAVITY))
#define E_SQUARED(Z) (OMEGA_M_0 * (1.0 + (Z)) * (1.0 + (Z)) * (1.0 + (Z)) + OMEGA_L_0)

// function prototypes
double radius_vir(double m_vir, double z);
double mass_vir(double r_vir, double z);
double overdensity_vir(double z);
