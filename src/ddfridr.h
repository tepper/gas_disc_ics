// provides derivative by Ridders' method (see Num.Rec., Ch. 5.7)

double ddfridr(double (*func)(double), double x, double h, double *err);