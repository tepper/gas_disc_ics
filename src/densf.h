// provides various normalised (i.e., dimensionless) density profile functions

// function prototypes
double nfw_density_func(double x);	// NFW (1997) density profile
double ein_density_func(double x);	// Einasto (1965) density profile
double miy_density_func(double r, double z);	// Miyamoto-Nagai (1975) density profile
double plu_density_func(double x); // Plummer (1911) density profile
double her_density_func(double x); // Hernquist (1990) density profile
double bur_density_func(double x); // Burkert (1995) density profile
double kuz_sfdensity_func(double x); // Kuzmin (1956) a.k.a. Toomre (1963) surface density
double exp_sfdensity_func(double x); // Exponential surface density
double uni_sfdensity_func(double x); // unity (dummy) surface density