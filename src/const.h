// provides physical constants and conversion factors

// conversion factors and mathematical constants
#define CM_PER_KM 1.0E5
#define CM_PER_PC 3.08567758E18
#define CM_PER_KPC (CM_PER_PC * 1.0E3)
#define CM_PER_MPC (CM_PER_PC * 1.0E6)
#define G_PER_MSUN 1.9891E33
#define PI (2.0 * asin(1.0))
#define SQRTPI sqrt(PI)
#define EULER 2.71828182

// Provides physical constants (cgs units); CODATA Mohr et al. (2010)
// and function macros
#define GRAVITY 6.67384E-8       // cm^3 / g s^2
#define CLIGHT 2.99792458E+10		// cm/s; speed of light
#define HPLANCK 6.62606957E-27	// erg/s; Planck's constant
#define BOLTZMANN 1.3806488E-16	// erg/K; Boltzmann's constant
#define PROTON_MASS 1.672621777E-24 // g; proton's mass

// MW DM halo mass [Msun], concentration (c or x_vir), gas disc mass [Msun], disc scale length and scale height [kpc] from Kafle et al. (2014; DM mass and x_vir (or c) from their figure 8; all other values from table 2)
// the name of the variables comes from the limiting cases of the MN model in the limits b -> 0 and a -> 0, respectively
#define MW_DM_HALO_MASS 1.0E12
#define MW_DISC_MASS 9.5E10
#define MW_X_VIR 15.0
#define A_KUZMIN 4.9
#define A_HERNQUIST 0.585 // Hernquist halo equivalent to NFW halo with c=40, R200=10, M200=2.32457e8 Msun
#define B_PLUMMER 0.3
#define NORM_KUZ (2.0 * A_KUZMIN / PI)

// solar bulk composition mass fractions (Asplund et al. 2009; their Sec. 3.12)
#define H_MASS_FRAC 0.7154
#define He_MASS_FRAC 0.2703
#define Z_MASS_FRAC 0.0142

// RSS (FYRIS) values of choice:
// 2007 Solar x0.1 correct values
// Use these for new asplundx0.1v2.txt models:
#define MU_PARTICLES 0.59049
#define MU_HI 1.3169
#define MU_IONS 1.2229
#define MU_ELECTRONS ( 1.0 / ( (1.0 / MU_PARTICLES) - (1.0 / MU_IONS) ) )