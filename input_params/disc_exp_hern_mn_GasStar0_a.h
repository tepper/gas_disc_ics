// PHYSICAL PARAMETERS (cgs units)

// #define OUT_DEBUG

#define UNIT_LENGTH CM_PER_KPC // cm <=> 1 kpc
#define UNIT_MASS G_PER_MSUN   // g  <=> 1 solar mass

// Gas disc properties. Note that the gas radial scale length, mean molecular weight and
// temperature are only relevant if an external potential component is included.
// IMPORTANT: define *EITHER* SIGMA0 *OR* DGAS_MASS. They are related through
//
//       M_gas = 2 PI Sigma_0 R_d^2
//
// assuming an exponential surface density profile.
//
#define SDENS "exp"        // gas disc surface density profile; add others yourself
#define DGAS_MASS 1.0E10   // gas disc mass [in units of UNIT_MASS]
// #define SIGMA0 3.0E-3      // gas disc central surface density [g/cm^2]
#define DGAS_SCALEL 3.5    // gas disc radial scale length R_d [in units of UNIT_LENGTH]
#define MU 1.0             // mean molecular weight
#define DGAS_TEMP 1.0E4    // gas disc temperature [Kelvin]


// External potential. To include one or both, define the corresponding potential type.
// In each case, the corresponding parameters MUST be defined as well. If the type
// is not defined, the corresponding parameters have no effect

// halo potential:
// NOTE 1: choose *only* one DM halo profile
// NOTE 2: to ignore a DM halo altogether, comment out ONLY
//  the definitions of DMTYPE; otherwise compilation will fail.
//  The same applies to SDISCTYPE.
// NOTE 3: scale factors are computed independently using
// ~/myCodes/potentials/halo_profiles/halo_scale_factors.c
// NOTE 4: NFW profile is problematic near 0, so use it with care.

// Hernquist
#define DMTYPE "her"          // DM profile: her = Hernquist; nfw = NFW; default: none
#define DM_HALOMASS 1.0E12    // DM *scale* mass: 4 pi M_s [in units of UNIT_MASS]
#define DM_SCALEL 33.0        // halo *scale* length r_s [in units of UNIT_LENGTH]

// NFW
// #define DMTYPE "nfw"          // DM profile: her = Hernquist; nfw = NFW; default: none
// #define DM_HALOMASS 5.44E11   // DM *scale* mass: 4 pi M_s [in units of UNIT_MASS]
// #define DM_SCALEL 17.2        // halo *scale* length r_s [in units of UNIT_LENGTH]

// stellar disc potential
// NOTE: to ignore a stella-disc potential, just comment out the definition of SDISCTYPE
// and ONLY the definition of SDISCTYPE

#define SDISCTYPE "mn"   // mn = Miyamoto-Nagai
#define SDISC_MASS 4.0E10    // stellar (disc) mass [in units of UNIT_MASS]
#define SDISC_SCALEL 3.5  // s-disc scale length [in units of UNIT_LENGTH]
#define SDISC_SCALEH 0.2  // s-disc scale height [in units of UNIT_LENGTH]



// Range and resolution of the output grid
// sR_GRID_MIN should ALWAYS be 0; otherwise strange things might happen...
// Change sR_GRID_MAX to whatever value appropriate for your problem,
// but note that depending on the choice of the surface density function, the
// accuracy of the numerical solution will decrease for 'large' values of sR_GRID_MAX, which is
// limited by both X_INT_INFTY and the choice of target surface density. For example,
// for an exponential decaying surface density profile exp[-sR], the value of the integral S
// will be exp[sR] at any given sR. Adopting X_INT_INFTY = 10^6, a value
// sR_GRID_MAX = 15 is already too large, since log(10^6) ~ 14. In other words,
// for sR_GRID_MAX >~ 14 the integral S will increasingly diverge from its true value,
// and so will the density.
// Also, do not chose sR_GRID_STEP too large, as this may increase the error
// of the numerical solution; values <= 0.1 seem fine. 
// Similar considerations apply to X_GRID_* below.
#define sR_GRID_MIN 0.0
// #define sR_GRID_MAX 15.0 // use this when scale-free (i.e. without external potential)
#define sR_GRID_MAX 51.0 //to cover a range in R = sqrt(2) * 125 kpc adopting R_d = 3.5 kpc
// choose one and only one of the following (if both defined, LIN has priority):
#define sR_GRID_STEP_LIN 0.01//0.05
// #define sR_GRID_STEP_LOG 0.0005

// note that the following actually refer to the scaled vertical distance (x)
// in other words, the values of this coordinate need to be rescaled
// by the appropriate z_0
#define X_GRID_MIN 0.0
// #define X_GRID_MAX 0.0
// #define X_GRID_MAX 1000.0 // use this when scale-free (i.e. without external potential)
#define X_GRID_MAX 2000.0 //to cover a range in z = 32 kpc adopting z_0 = 1.64527212E-02 kpc
// #define X_GRID_MAX 7598.0 //to cover a range in z = 125 kpc adopting z_0 = 1.64527212E-02 kpc
// #define X_GRID_MAX 1320.0 //to cover a range in z = 125 kpc adopting z_0 = 0.0949 kpc; should be 1330!
#define X_GRID_STEP 1.0
