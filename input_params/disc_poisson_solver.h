// ATTENTION: The following parameters are better left unchanged! But if you feel you need to, do so at your own risk.

// The following define the integration range of Poisson's equation over x, the precision of the numerical solution, and the resolution of the solution on the grid for the scaled vertical coordinate. All these affect the integration via odeint. Note that a more frequent saving leads to better solution, since the error introduced when interpolating in order to compute the surface density normalisation is smaller. That being said, values of 1.0e-4 or smaller should be fine.
#define X_INT_ORIGIN 0.0 //FLT_MIN
#define X_INT_INFTY 1.0E6   // this needs to be =< MAXSTP in dodeint.c; note that its value is limited by the available memory
#define X_INT_STEP 1.0E-4
#define X_INT_STEP_MIN 0.0
#define PRECISION 1.0E-16
#define X_INT_STEP_SAVE 1.0E-4

// 'CONVERGENCE' defines the convergence criterium: if the *relative* change
// of the surface density normalisation is strictly smaller than this value, the
// solution has converged. However, the code won't iterate more than ITER_MAX
// times. If convergence is not achieved, you may need to increase ITER_MAX to within
// reasonable values. For reference, around 20 steps are required to reach convergence
// at the 1.0e-6 level (see below). Obviously, more steps might be required
// for more stringent convergence criteria. If the number of steps is not large enough,
// the convergence criterium may be instead be softened. i.e. increase CONVERGENCE by
// factors 10x.
//
// For the case of an exponential surface density profile and no external potential,
// the number of iterations N needed to attain convergence at a level K starting from
// any reasonable guess for sS (e.g. 1 but also 100) can be estimated by
//
//       N + 1 = log{ -3 log[2] / log[1 - K] } / log[2]
//
// where log is the logarithm in any base: 10, e, 2,... . The inverse formula is
//
//       1 - K = 2^{ -3 / 2^{N+1} }
//
// So, for example, K = 1.0e-6 gives N ~ 20. Note that adding an external potential
// decreases the required number of iterations. For example, adding a DM potential
// of Hernquist type for a halo of 1.0E12 Msun, only 7 - 8 iterations are needed
// to reach convergence at the 1.0E-6 level, which is comparable to the N ~ 6 of
// in Wang et al. 2010).
// Note that N should be fairly independent of R; an decrease of N for increasing R
// indicates an under/overflow of the integral. For these values of R, the solution
// will no longer be accurate enough, or even correct.
//
#define CONVERGENCE 1.0E-6
#define ITER_MAX 100


// The following affect the computation of the potential in the equatorial plane;
// better not to change these!
#define A_MIN sR_GRID_MIN
#define A_MAX sR_GRID_MAX
#define A_STEP 0.01
#define A_GRID_PTS ((A_MAX - A_MIN)/A_STEP + 1)
