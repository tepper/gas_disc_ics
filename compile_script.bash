#!/bin/bash

CC=gcc

echo "issuing command:"
echo "rm -f disc_poisson_solver; cd src/.; ${CC} disc_poisson_solver.c nrutil.c rkqs.c rkck.c dodeint.c dspline.c dsplint.c ddfridr.c dbessi0.c dbessi1.c dbessk0.c dbessk1.c cosmo.c densf.c alloc_funcs.c -o disc_poisson_solver; mv disc_poisson_solver ../.; cd ../."

rm -f disc_poisson_solver; cd src/.; ${CC} disc_poisson_solver.c nrutil.c rkqs.c rkck.c dodeint.c dspline.c dsplint.c ddfridr.c dbessi0.c dbessi1.c dbessk0.c dbessk1.c cosmo.c densf.c alloc_funcs.c -o disc_poisson_solver; mv disc_poisson_solver ../.; cd ../.
